VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisWorkbook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit

Public Enum eFIDList
  No = 1
  Fid = 2
  FilterName = 3
  FSet_Project = 4
  FSet_Type = 5
  FSet_Resolution = 6
  FSet_PrimeArea = 7
  FSet_Resolved = 8
  SearchFormula = 9
End Enum


Private Sub Workbook_Open()
  Dim ws As Worksheet

  For Each ws In Worksheets
    If (ws.Name = gFIDListSheetName) Then
      Call MakeFIDList(ws)
    End If
  Next ws

  Worksheets(gConfigSheetName).Activate
  ThisWorkbook.Save
End Sub

Public Function MakeFIDList(ByVal ws As Worksheet)
  Const START_ROW As Long = 2
  Dim items() As Variant
  Dim rowMax As Long

  With ws
    ' FID列の最終行を求める
    rowMax = .Cells(Rows.Count, eFIDList.Fid).End(xlUp).Row

    items = WorksheetFunction.Transpose(.Range(.Cells(START_ROW, eFIDList.Fid), _
                                               .Cells(rowMax, eFIDList.Fid)).Value)
  End With

  With Worksheets(gConfigSheetName).Range(gFIDRangeDef).Validation
    .Delete
    .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Formula1:=Join(items, ",")
  End With
End Function
