VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ProgressInfo 
   Caption         =   "Please wait for a while."
   ClientHeight    =   3045
   ClientLeft      =   120
   ClientTop       =   435
   ClientWidth     =   4560
   OleObjectBlob   =   "ProgressInfo.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "ProgressInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub UserForm_Initialize()

  With ProgressBar1
    .Min = 0
    .Max = 100
    .Value = 0
  End With

  Percent.Caption = ""
 
End Sub
