Attribute VB_Name = "BatchExe"
Option Explicit

'*******************************************************************************
' バッチ実行(JIRAデータ取得＆JIRAデータ統合)マクロ
'
' [引数]
'   なし
'
' [戻り値]
'   なし
'
' [処理概要]
'   (Step 1)「FID_List」シートにある全FID値を取得する。
'           ※取得不可の場合、強制終了
'   (Step 2)全FIDに対して、「JIRA Import」処理を行い、データ取得する。
'   (Step 3)「Target Date」に現在の日付を記載する。
'   (Step 4)「Integrate Data」処理を行い、データ統合処理を行う。
'*******************************************************************************
Public Sub ExecuteBatch()
  Dim StartTime As Double
  Dim EndTime As Double
  Dim TotalTime As Double
  Dim wsConf As Worksheet: Set wsConf = Worksheets(gConfigSheetName)
  Dim strOrgFID As String
  Dim i As Long


  ' 開始時間取得
  StartTime = Timer

  ' プログレスバーFormを表示
  ProgressInfo.Show vbModeless

  Application.ScreenUpdating = False
  Application.Calculation = xlCalculationManual

  ' 現状、記載されているFIDを一時保存しておく。
  strOrgFID = wsConf.Range(gFIDRangeDef).Value
  Debug.Print "(ExecuteBatch):strOrgFID = " & strOrgFID

  ' (Step 1)「FID_List」シートにある全FID値を取得する。
  If (GetAllFid(gFIDListSheetName) = False) Then
    ' プログレスバーFormを閉じる
    Unload ProgressInfo
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    ' ※取得不可の場合、強制終了
    MsgBox "[" & gFIDListSheetName & "]シートが存在しないか、" & vbCrLf & _
           "シート上にFIDの記載がありません。", vbCritical
    Exit Sub
  End If

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Get all FID-list finished.")
  Call UpdateProgressBar(10)

  ' (Step 2)全FIDに対して、「JIRA Import」処理を行い、データ取得する。
  For i = 1 To gIntegSet.TotalFid
    wsConf.Range(gFIDRangeDef).Value = gIntegSet.Fid(i)
    Debug.Print "(ExecuteBatch):GetJiraData(Fid = " & gIntegSet.Fid(i) & ")"

    Call GetJiraData(False)
    ' 1秒Wait
    Application.Wait Now() + TimeValue("00:00:01")

    Call UpdateProgressBar(10 + (i / gIntegSet.TotalFid) * 70)
  Next i

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "GetJiraData for all FID finished.")

  ' (Step 3)Target Dateに現在の日付を記載する。
  wsConf.Range(gTargetDateRangeDef).Value = Format(Now, "yyyymmdd")

  ' (Step 4)「Integrate Data」処理を行い、データ統合処理を行う。
  Call IntegrateJiraData(False)

  ' FID値を元に戻しておく。
  wsConf.Range(gFIDRangeDef).Value = strOrgFID

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "IntegrateJiraData finished.")
  Call UpdateProgressBar(100)

  ' プログレスバーFormを閉じる
  Unload ProgressInfo
  Application.ScreenUpdating = True
  Application.Calculation = xlCalculationAutomatic

  ' 終了時間取得＆Total時間算出
  EndTime = Timer
  TotalTime = EndTime - StartTime

  MsgBox "Batch processing completed." & vbCrLf & _
         "elapsed time : " & TotalTime & " (sec)", vbInformation
End Sub

' プログレスバーの処理進捗率を更新するFunction
' [引数]
'   percent As Long：プログレスバーに表示する％値
'
' [戻り値]
'   なし
Public Function UpdateProgressBar(ByVal Percent As Long)
  With ProgressInfo
    .ProgressBar1.Value = Percent
    .Percent.Caption = Int(Percent / .ProgressBar1.Max * 100) & "%"
    DoEvents
  End With
End Function

' 途中までの経過時間をデバッグ表示するFunction
' [引数]
'   StartTime As Double ：処理開始時間
'   strDispMsg As String：経過時間の前に付与するラベル文字列
'
' [戻り値]
'   なし
Public Function DispElapsedTime(ByVal StartTime As Double, _
                                ByVal strDispMsg As String)
  Dim MiddleTime As Double
  Dim ProcessTime As Double

  MiddleTime = Timer
  ProcessTime = MiddleTime - StartTime
  Debug.Print strDispMsg; ProcessTime

End Function
