Attribute VB_Name = "MyLib"
Option Explicit

' 指定文字列が変換可能な日付かチェックするFunction
'
' [引数]
'   str As String：日付文字列
'
' [戻り値]
'   Boolean：True  (変換可能)
'          ：False (変換不可)
Function IsDateEx(ByVal str As String) As Boolean
  Dim i As Long
  Dim sDate As String
  Dim sTemp As String


  sDate = ""

  ' 数字のみを抽出
  For i = 0 To Len(str)
    sTemp = Mid(str, i + 1, 1)

    ' 数字の場合
    If (IsNumeric(sTemp) = True) Then
      sDate = sDate & sTemp
    End If
  Next

  ' 数字8文字でない場合は不正とみなす
  If (Len(sDate) <> 8) Then
    IsDateEx = False
    Exit Function
  End If

  ' 日付形式に変換
  sDate = Format(sDate, "####/##/##")

  ' 日付チェック
  IsDateEx = IsDate(sDate)

End Function

' 指定シートを別シートにコピーするFunction
'
' [引数]
'   SrcSheet As String：コピー元のシート名
'   DstSheet As String：コピー先のシート名
'
' [戻り値]
'   なし
Public Function CopySheetRange(ByVal SrcSheet As String, _
                               ByVal DstSheet As String)
  Dim wsSrc As Worksheet
  Dim wsDst As Worksheet


  Debug.Print "(CopySheet):SrcSheet = " & SrcSheet & _
              " DstSheet = " & DstSheet
  Set wsSrc = Worksheets(SrcSheet)
  Set wsDst = Worksheets(DstSheet)
  wsSrc.Range("A1").CurrentRegion.Copy wsDst.Range("A1")

End Function

' 指定シートを別シートにコピーするFunction
'
' [引数]
'   wsSrc As Worksheet：コピー元のワークシートオブジェクト
'   wsDst As Worksheet：コピー先のワークシートオブジェクト
'
' [戻り値]
'   なし
Public Function CopyWs2Ws(ByVal wsSrc As Worksheet, _
                          ByVal wsDst As Worksheet)
  Const START_ROW As Long = 2
  Dim rowLastSrc As Long
  Dim rowLastDst As Long
  Dim strRngSrc As String
  Dim strRngDst As String


  Debug.Print "(CopyWs2Ws):wsSrc.Name = " & wsSrc.Name & _
              " wsDst.Name = " & wsDst.Name

  rowLastSrc = wsSrc.Cells(Rows.Count, 1).End(xlUp).Row
  rowLastDst = wsDst.Cells(Rows.Count, 1).End(xlUp).Row

  Debug.Print "(CopyWs2Ws):rowLastSrc = " & rowLastSrc & _
              " rowLastDst = " & rowLastDst

  strRngSrc = START_ROW & ":" & rowLastSrc
  strRngDst = (rowLastDst + 1) & ":" & (rowLastDst + 1)

  Debug.Print "(CopyWs2Ws):strRngSrc = " & strRngSrc & _
              " strRngDst = " & strRngDst

  wsSrc.Range(strRngSrc).Copy wsDst.Range(strRngDst)

End Function

' 指定シートを作成するFunction
' (既に同名シートがある場合は、削除後に再度新規作成する)
'
' [引数]
'   strSheetName As String：作成するシート名
'
' [戻り値]
'   なし
Public Function CreateSheet(ByVal strSheetName As String)
  Debug.Print "(CreateSheet):strSheetName = " & strSheetName

  ' 既に同名シートがある場合、削除する
  If isSheetExist(strSheetName) = True Then
    Application.DisplayAlerts = False
    Worksheets(strSheetName).Delete
    Application.DisplayAlerts = True
  End If

  Worksheets.Add After:=Worksheets(Worksheets.Count)
  ActiveSheet.Name = strSheetName

  ' **** シートの書式設定 ****
  ' Font種別 = Meiryo UI
  Cells.Font.Name = "Meiryo UI"

End Function

' 指定シートが存在するかチェックするFunction
'
' [引数]
'   strSheetName As String：シート名
'
' [戻り値]
'   Boolean：True  (指定シートが存在する)
'          ：False (指定シートが存在しない)
Public Function isSheetExist(ByVal strSheetName As String) As Boolean
  Dim ws As Worksheet
  Dim FindFlg As Boolean

  FindFlg = False

  For Each ws In Worksheets
    If ws.Name = strSheetName Then
      FindFlg = True
      Exit For
    End If
  Next ws

  If FindFlg = False Then
    isSheetExist = False
    Exit Function
  End If

  isSheetExist = True

End Function

' 指定シートの各種パラメータ設定を行うFunction
'
' [引数]
'   ws As Worksheet：対象ワークシート
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function SetWsParam(ByVal wsTarget As Worksheet)
  Dim wsRtn As Worksheet: Set wsRtn = ActiveSheet

  With wsTarget
    ' 指定シートへ切替
    .Activate
    ' シート全体の行/列幅を設定
    .Cells.RowHeight = 25
    .Cells.ColumnWidth = 20
    ' シート全体のフォント名/サイズを設定
    .Cells.Font.Name = "Meiryo UI"
    .Cells.Font.Size = 9
    ' シートの表示倍率を設定
    ActiveWindow.Zoom = 100
    ' 先頭行を固定
    .Range("B2").Select
    ActiveWindow.FreezePanes = True
  End With
  ' 元のシートへ切替
  wsRtn.Activate

End Function

'--------------------------------------------------------------
' テーブル取得
'--------------------------------------------------------------
Public Function GetTableByName(ByVal TableName As String, _
                               ByVal wsTarget As Worksheet, _
                               Optional ByVal NoMsg As Boolean = False) As ListObject
  Dim objTmp As ListObject
  Dim objFound As ListObject

  Set objFound = Nothing

  For Each objTmp In wsTarget.ListObjects
    If (objTmp.Name = TableName) Then
      Set objFound = objTmp
      Exit For
    End If
  Next objTmp

  If (objFound Is Nothing) And (NoMsg = False) Then
    MsgBox wsTarget.Name & "シートに" & _
           "「" & TableName & "」というテーブルがありません。", vbCritical
  End If

  Set GetTableByName = objFound
End Function

'--------------------------------------------------------------
' シート名でワークシートを取り出す
'--------------------------------------------------------------
Public Function GetWorkSheetByName(ByVal SearchName As String, _
                                   ByVal TargetBook As Workbook, _
                                   Optional ByVal NoMsg As Boolean = False) As Worksheet
  Dim objWS As Worksheet

  For Each objWS In TargetBook.Worksheets
    If objWS.Name = SearchName Then
      Set GetWorkSheetByName = objWS
      Exit Function
    End If
  Next objWS

  If (NoMsg = False) Then
    MsgBox TargetBook.Name & vbCrLf & _
           "に""" & SearchName & """シートがありません。", vbCritical
  End If

  Set GetWorkSheetByName = Nothing
End Function

'=============================================================
'
'    名前定義の処理
'
'=============================================================
Public Function GetRefNameRange(ByVal ws As Worksheet, _
                                ByVal strName As String, _
                                Optional NoMsg As Boolean = False) As Range
  If ws Is Nothing Then
    Call MsgBox("Bug!!", vbCritical)
    Stop
  End If

  On Error Resume Next

  Set GetRefNameRange = ws.Range(strName)

  If Err.Number <> 0 Then
    If Not NoMsg Then
      MsgBox "'" & ws.Name & "'シートに'" & strName & "'の名前定義がありません", vbCritical
    End If

    Err.Clear
    Set GetRefNameRange = Nothing
  End If

  On Error GoTo 0

End Function

'=============================================================
'
'    文字列処理
'
'=============================================================
Public Function IsSpaceString(ByVal strValue As String) As Boolean
  ' 空白文字の除去
  If MySpaceCharsReplace(strValue, "") = "" Then
    IsSpaceString = True
  Else
    IsSpaceString = False
  End If
End Function

Public Function MySpaceCharsReplace(ByVal ConvTarget As String, _
                                    ByVal SubChar As String) As String
  Dim strTmp As String
  Dim i As Long

  strTmp = ConvTarget

  ' 空白文字を半角空白に置換
  strTmp = Replace(strTmp, "　", " ") '全角空白
  strTmp = Replace(strTmp, vbTab, " ")
  strTmp = Replace(strTmp, vbLf, " ")
  strTmp = Replace(strTmp, vbCr, " ")
  strTmp = Replace(strTmp, vbBack, " ")

  '連続する空白を１文字に変換
  Do
    i = Len(strTmp)
    strTmp = Replace(strTmp, "  ", " ")
  Loop While Len(strTmp) <> i

  MySpaceCharsReplace = Replace(strTmp, " ", SubChar)
End Function


