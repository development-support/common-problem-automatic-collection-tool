Attribute VB_Name = "JiraGet"
Option Explicit

Public Const gConfigSheetName As String = "Operation"
Public Const gJIRASheetName As String = "JIRA_RawData"
Public Const gSettingSheetName As String = "Setting"
Public Const gFIDListSheetName As String = "FID_List"
Public Const gFIDRangeDef As String = "ConfigFilterID"
Public Const gFilterNameRangeDef As String = "FilterName"
Public Const gFIDListTable As String = "FIDListTable"
Public Const gBaseURLRangeDef As String = "BaseURL"
Public Const gUserIDRangeDef As String = "UserID"
Public Const gPasswordRangeDef As String = "Password"
Public Const gJiraImportBtn As String = "Jira_Import_Btn"
Public Const gFuncImportBtn As String = "Func_Import_Btn"
Public Const gCheckBoxFidAll As String = "CheckBox_FID_All"
Public Const gCheckBoxFidCur As String = "CheckBox_FID_Cur"

' 日付変更が必要なJIRAデータのField name定義
Public Const gDateConvField As String = _
  "Created,Last Viewed,Updated,Resolved,Due Date,Done Date,Target Fix Date,Target Analysis Date"

' JIRA PTR Listのフィールド設定定義
Public Enum eFieldsSet
  Error = 0
  All = 1
  Current = 2
End Enum

Type tFIDSet
  Id As String           ' フィルターID
  OutputPath As String   ' 出力パス名(ファイル名込み)
  FieldsOp As eFieldsSet ' 選択フィールド状態
  SearchField As String  ' 絞り込み検索対象のフィールド(見出し)名
  SearchStr As String    ' 絞り込み検索対象の文字列
End Type

Type tBaseInfo
  BaseURL As String  ' JIRA Base URL
  UserID As String   ' JIRA User ID
  Password As String ' JIRA User Pass
End Type

Public gSettingFID As tFIDSet
Public gSettingBase As tBaseInfo


'*******************************************************************************
' 自動実行用マクロ
'
' [引数]
'   なし
'
' [戻り値]
'   なし
'
' [処理概要]
'   (Step 1)Curlで指定フィルタIDのJIRAデータを取得する。
'*******************************************************************************
Public Sub GetJiraData(Optional ByVal bEndMessage As Boolean = True)
Attribute GetJiraData.VB_ProcData.VB_Invoke_Func = " \n14"
  Const strFilename As String = "\JiraExport.xls"
  Dim wsHome As Worksheet
  Dim strCurlCmd As String
  Dim strOutputSheet As String
  Dim strNDownSheet As String


  Application.ScreenUpdating = False
  Application.Calculation = xlCalculationManual

  ' JIRA_Importボタンがあるシート設定
  Set wsHome = Worksheets(gConfigSheetName)
  gSettingFID.OutputPath = ActiveWorkbook.Path & strFilename
  Debug.Print "(JiraGet):gSettingFID.OutputPath = " & gSettingFID.OutputPath

  ' 基本設定情報を取得
  If (GetBaseInfo() = False) Then
    MsgBox "[強制終了]基本情報(JIRA BASE URL/UserID/Password)を指定して下さい。", vbCritical
    Exit Sub
  End If
  ' FIDを取得
  If (GetModeFID() = False) Then
    MsgBox "[強制終了]Filter IDを指定して下さい。", vbCritical
    Exit Sub
  End If
  ' 指定フィールド情報を取得
  If (GetModeFields() = False) Then
    MsgBox "[強制終了]不正なfields指定です。", vbCritical
    Exit Sub
  End If

  ' Curlコマンド経由でJIRAデータを取得
  Dim ret As Long

  With CreateObject("Wscript.Shell")
    strCurlCmd = MakeCurlCmd(gSettingFID.OutputPath, _
                             gSettingFID.Id, _
                             gSettingFID.FieldsOp)

    ' all
    ret = .Run(strCurlCmd, 7, True)
  End With

  If (ret <> 0) Then
    MsgBox "JIRAデータのダウンロードに失敗しました" & vbCrLf & _
           "(ret :" & ret & ")", vbCritical
    Exit Sub
  End If

  strOutputSheet = gJIRASheetName
  Debug.Print "(JiraGet):strOutputSheet = " & strOutputSheet

  ' シートを作成する。
  Call CreateSheet(strOutputSheet)

  ' Curlで取得したJIRA Data Fileをシートにコピー
  If (CopyJIRADataToSheet(strFilename, strOutputSheet) = False) Then
    MsgBox "CopyJIRADataToSheet failed!", vbCritical
    Exit Sub
  End If

  strNDownSheet = gSettingFID.Id & "_" & Format(Now, "yyyymmdd")

  ' シートを作成する。
  Call CreateSheet(strNDownSheet)

  ' FID_Listシートに記載のLinked Issue条件で、チケットを絞り込む
  If (NarrowDownByFields(strOutputSheet, _
                         gFIDListSheetName) = False) Then
    MsgBox "NarrowDownByFields failed!", vbCritical
    Exit Sub
  End If

  ' 絞り込んだ結果を出力用シートにコピー
  Call CopySheetRange(strOutputSheet, strNDownSheet)
  Call SetWsParam(Worksheets(strNDownSheet))

  Worksheets(strOutputSheet).Visible = False

  Application.Calculation = xlCalculationAutomatic
  Application.ScreenUpdating = True

  ' 元のシートへ戻る
  wsHome.Activate

  If (bEndMessage = True) Then
    MsgBox "Processing completed.", vbInformation
  End If

End Sub

' 設定パラメータ(基本情報)を取得するFunction
'
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗[基本情報の指定無し])
Public Function GetBaseInfo() As Boolean

  With Worksheets(gSettingSheetName)
    gSettingBase.BaseURL = .Range(gBaseURLRangeDef).Value
    gSettingBase.UserID = .Range(gUserIDRangeDef).Value
    gSettingBase.Password = .Range(gPasswordRangeDef).Value
  End With

  Debug.Print "(GetBaseInfo):gSettingBase.BaseURL = " & gSettingBase.BaseURL & vbCrLf & _
              "              gSettingBase.UserID = " & gSettingBase.UserID & vbCrLf & _
              "              gSettingBase.Password = " & gSettingBase.Password

  ' **** 実行条件チェック ****
  ' BaseURL, UserID, Passwordいずれかが指定されていない場合、エラー終了
  If (gSettingBase.BaseURL = "") Or _
     (gSettingBase.UserID = "") Or _
     (gSettingBase.Password = "") Then
    GetBaseInfo = False
    Exit Function
  End If

  GetBaseInfo = True

End Function

' 設定パラメータ(FID情報)を取得するFunction
'
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗[FID指定無し])
Public Function GetModeFID() As Boolean

  With Worksheets(gConfigSheetName)
    ' Filter IDを取得
    gSettingFID.Id = .Range(gFIDRangeDef).Value
  End With

  Debug.Print "(GetModeFID):gSettingFID.Id = " & gSettingFID.Id

  ' **** 実行条件チェック(FIDが指定されているか) ****
  ' FIDが指定されていない場合、エラー終了
  If (gSettingFID.Id = "") Then
    GetModeFID = False
    Exit Function
  End If

  GetModeFID = True

End Function

' 設定パラメータ(fields情報)を取得するFunction
'
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗)
Public Function GetModeFields() As Boolean

  With Worksheets(gConfigSheetName)
    ' Fieldsオプション指定を取得
    If (.CheckBoxes(gCheckBoxFidAll).Value = xlOn) And _
       (.CheckBoxes(gCheckBoxFidCur).Value = xlOff) Then
      gSettingFID.FieldsOp = eFieldsSet.All
    ElseIf (.CheckBoxes(gCheckBoxFidAll).Value = xlOff) And _
           (.CheckBoxes(gCheckBoxFidCur).Value = xlOn) Then
      gSettingFID.FieldsOp = eFieldsSet.Current
    ElseIf (.CheckBoxes(gCheckBoxFidAll).Value = xlOn) And _
           (.CheckBoxes(gCheckBoxFidCur).Value = xlOn) Then
      gSettingFID.FieldsOp = eFieldsSet.Error
    ElseIf (.CheckBoxes(gCheckBoxFidAll).Value = xlOff) And _
           (.CheckBoxes(gCheckBoxFidCur).Value = xlOff) Then
      gSettingFID.FieldsOp = eFieldsSet.Error
    End If
  End With

  Debug.Print "(GetModeFields):gSettingFID.FieldsOp = " & gSettingFID.FieldsOp

  If (gSettingFID.FieldsOp = Error) Then
    GetModeFields = False
    Exit Function
  End If

  GetModeFields = True

End Function

' シート内容を全クリアするFunction
'
' [引数]
'   strSheetName As String：内容を全クリアするシート名
'
' [戻り値]
'   なし
Private Function ClearSheet(ByVal strSheetName As String)
  Dim ws As Worksheet


  Debug.Print "(ClearSheet):strSheetName = " & strSheetName

  ' 同名シートが存在する場合、一旦削除する。
  For Each ws In Worksheets
    If (ws.Name = strSheetName) Then
      Application.DisplayAlerts = False
      Worksheets(strSheetName).Delete
      Application.DisplayAlerts = True
    End If
  Next ws

  ' シートを新規追加
  Worksheets.Add After:=Worksheets(Worksheets.Count)
  ActiveSheet.Name = strSheetName

  ' シート全体の表示形式を文字列に設定
  Cells.NumberFormatLocal = "@"

End Function

' 指定シートの指定見出し列の日付を変換するFunction
'
' [引数]
'   strSheetName As String：日付変換対象シート名
'
' [戻り値]
'   なし
Private Function DateConvSheet(ByVal strSheetName As String)
  Const START_ROW As Long = 2
  Dim wsCur As Worksheet
  Dim vField As Variant
  Dim i As Long
  Dim j As Long
  Dim MaxRow As Long
  Dim strFieldName As String
  Dim rowTemp As Long
  Dim colTemp As Long
  Dim strCellAddr As String

  Debug.Print "(DateConvSheet):strSheetName = " & strSheetName
  Set wsCur = Worksheets(strSheetName)

  wsCur.Activate

  vField = Split(gDateConvField, ",")

  '使用済み最終行を取得
  With wsCur.UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
  End With

  For i = LBound(vField) To UBound(vField)
    Debug.Print "(DateConvSheet):vField(" & i & ") = " & vField(i)
    strFieldName = vField(i)

    ' 日付変更対象フィールド見出しの行/列を取得
    If (GetHeadPos(strSheetName, _
                   strFieldName, _
                   rowTemp, colTemp) = False) Then
      Debug.Print "(DateConvSheet):GetHeadPos Error!" & vbCrLf & _
                  "                strSheetName = " & strSheetName & _
                  " strFieldName = " & strFieldName
      GoTo DATA_CONV_SKIP
    End If

    Debug.Print "(DateConvSheet):rowTemp = " & rowTemp & _
                " colTemp = " & colTemp

    ' 日付の値が入力されているセルのみ、表示形式を日付に設定する
    For j = START_ROW To MaxRow
      strCellAddr = wsCur.Cells(j, colTemp).Address

      With wsCur
        If (.Range(strCellAddr).Value <> "") Then
          .Range(strCellAddr).NumberFormatLocal = "yyyy/m/d"
        End If
      End With
    Next j
DATA_CONV_SKIP:
  Next i

End Function

' JIRAデータファイルをシートにコピーするFunction
'
' [引数]
'   strJIRADataFile As String：Curlで取得したJIRA Dataファイル名
'   strSheetName As String：JIRA Dataファイル内容のコピー先シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Private Function CopyJIRADataToSheet(ByVal strJIRADataFile As String, _
                                     ByVal strSheetName As String) As Boolean
  Dim wb As Workbook
  Dim buf As String
  Dim Target As Workbook
  Dim Original As Workbook
  Dim wsCopyDist As Worksheet
  Dim i As Long
  Dim MaxRow As Long
  Dim MaxCol As Long
  Dim strPath As String
  Const JIRA_START_ROW As Long = 4
  Dim rEmptyRow As Range
  Dim LastRow As Long

  Debug.Print "(CopyJIRADataToSheet):strJIRADataFile = " & strJIRADataFile & _
              " strSheetName = " & strSheetName

  ' 指定シートの内容を全クリア
  Call ClearSheet(strSheetName)

  Set Original = ThisWorkbook
  Set wsCopyDist = Original.Worksheets(strSheetName)

  strPath = ThisWorkbook.Path & "\" & strJIRADataFile
  Debug.Print "(CopyJIRADataToSheet):strPath = " & strPath

  ' ファイルの存在チェック
  buf = Dir(strPath)
  If buf = "" Then
    MsgBox strPath & vbCrLf & _
           "は存在しません", vbExclamation
    
    CopyJIRADataToSheet = False
    Exit Function
  End If

  ' 同名ブックのチェック
  For Each wb In Workbooks
    If wb.Name = buf Then
      MsgBox buf & vbCrLf & "は既に開いています", vbExclamation

      CopyJIRADataToSheet = False
      Exit Function
    End If
  Next wb

  ' Open JIRA Data File
  ' Excel読込中のエラー(セルのデータが大きすぎます)が出る場合の対策。
  Application.DisplayAlerts = False
  Set Target = Workbooks.Open(FileName:=strPath)
  Application.DisplayAlerts = True

  ' 使用済み最終行＆列を取得
  With Target.Sheets(1).UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    MaxCol = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Original.Activate

  For i = JIRA_START_ROW To MaxRow - 1
    ' Copy JIRA Data File to specified sheet
    Target.Sheets(1).Cells(i, 1).Resize(, MaxCol).Copy _
      wsCopyDist.Cells(i - (JIRA_START_ROW - 1), 1)
  Next

  ' 結合セルがあるフィールド列を削除する
  For i = MaxCol To 1 Step -1
    With wsCopyDist.Columns(i)
      If .Rows(1).MergeCells Then
        .Delete
      End If
    End With
  Next i

  ' Close JIRA Data File
  Target.Close SaveChanges:=False

  Call DateConvSheet(strSheetName)

  Call SetWsParam(wsCopyDist)

  LastRow = wsCopyDist.Cells(Rows.Count, 1).End(xlUp).Row
  With ActiveSheet
    For i = 2 To LastRow
      ' i行/1列目のセルが空白だった場合
      If IsEmpty(Cells(i, 1).Value) Then
        ' 最初の空白行
        If rEmptyRow Is Nothing Then
          Set rEmptyRow = .Rows(i).EntireRow
        ' 2件目以降の空白行
        Else
          Set rEmptyRow = Union(rEmptyRow, .Rows(i).EntireRow)
        End If
      End If
    Next i
  End With

  ' 空白行があれば一括で削除する
  If Not rEmptyRow Is Nothing Then
        rEmptyRow.Delete
  End If

  CopyJIRADataToSheet = True

End Function

' 指定シートにおいて、指定文字列(見出し)のセル列を特定するFunction
'
' [引数]
'   strSheetName As String：検索対象シート名
'   strHead As String     ：見出し文字列
'   rowNum As Long        ：行番号を格納する変数
'   colNum As Long        ：列番号を格納する変数
'
' [戻り値]
'   Boolean：True  (指定文字列の見出しセル列取得に成功)
'          ：False (指定文字列の見出しセル列取得に失敗)
Public Function GetHeadPos(ByVal strSheetName As String, _
                           ByVal strHead As String, _
                           ByRef rowNum As Long, _
                           ByRef colNum As Long) As Boolean
  Dim wsRtn As Worksheet
  Dim FoundCell As Range
  Dim i As Long

  Set wsRtn = ActiveSheet
  ' 検索対象シートに切替
  Worksheets(strSheetName).Activate

  Set FoundCell = Cells.Find(what:=strHead, LookAt:=xlWhole)
  If FoundCell Is Nothing Then
    rowNum = 0
    colNum = 0
    Debug.Print "(GetHeadPos):strSheetName = " & strSheetName & _
                " strHead = " & strHead & " rowNum = " & rowNum & _
                " colNum = " & colNum
    GetHeadPos = False
    ' 元のシートに切替
    wsRtn.Activate
    Exit Function
  End If

  rowNum = FoundCell.Row
  colNum = FoundCell.Column

  ' 元のシートに切替
  wsRtn.Activate

  GetHeadPos = True

End Function

' 指定したパラメータを基にCurlコマンドを生成するFunction
'
' [引数]
'   strPath As String     ：Curlで取得したデータの保存先パス
'   strFID As String      ：Curlで取得する際の対象FID
'   FieldsOp As eFieldsSet：Curlで取得する際の対象フィールド
'
' [戻り値]
'   String：生成したCurlコマンド文字列
Private Function MakeCurlCmd(ByVal strPath As String, _
                             ByVal strFID As String, _
                             ByVal FieldsOp As eFieldsSet) As String
  Dim strCmd As String

  Select Case FieldsOp
    Case eFieldsSet.All
      strCmd = "curl -Lkv -o """ & strPath & _
               """ -v -H ""Content-Type: application/json"" -X GET -u " & gSettingBase.UserID & ":" & _
               gSettingBase.Password & " " & _
               "" & gSettingBase.BaseURL & "/sr/jira.issueviews:searchrequest-html-all-fields/" & _
               strFID & "/SearchRequest-" & strFID & ".html?"""
    Case eFieldsSet.Current
      strCmd = "curl -Lkv -o """ & strPath & _
               """ -v -H ""Content-Type: application/json"" -X GET -u " & gSettingBase.UserID & ":" & _
               gSettingBase.Password & " " & _
               "" & gSettingBase.BaseURL & "/sr/jira.issueviews:searchrequest-html-current-fields/" & _
               strFID & "/SearchRequest-" & strFID & ".html?"""
    Case Else
      strCmd = "curl -Lkv -o """ & strPath & _
               """ -v -H ""Content-Type: application/json"" -X GET -u " & gSettingBase.UserID & ":" & _
               gSettingBase.Password & " " & _
               "" & gSettingBase.BaseURL & "/sr/jira.issueviews:searchrequest-html-all-fields/" & _
               strFID & "/SearchRequest-" & strFID & ".html?"""
  End Select

  Debug.Print "(MakeCurlCmd):strCmd = " & vbCrLf & strCmd
  MakeCurlCmd = strCmd

End Function

'*******************************************************************************
' JIRAデータ取得用のオプション選択時のイベント処理
'
' [引数]
'   なし
' [戻り値]
'   なし
'
' [処理概要]
'   (1)該当チェックボックスがONされた際、ペアとなるチェックボックスをOFFにする。
'      該当チェックボックスがOFFされた際、ペアとなるチェックボックスをONにする。
'*******************************************************************************
Private Sub CheckBox_FID_All_Change()
  With ActiveSheet
    If (.CheckBoxes(gCheckBoxFidAll).Value = xlOn) Then
      .CheckBoxes(gCheckBoxFidCur).Value = xlOff
    Else
      .CheckBoxes(gCheckBoxFidCur).Value = xlOn
    End If
  End With
End Sub

Private Sub CheckBox_FID_Cur_Change()
  With ActiveSheet
    If (.CheckBoxes(gCheckBoxFidCur).Value = xlOn) Then
      .CheckBoxes(gCheckBoxFidAll).Value = xlOff
    Else
      .CheckBoxes(gCheckBoxFidAll).Value = xlOn
    End If
  End With
End Sub

' 指定条件でチケットを絞り込むFunction
'
' [引数]
'   TargetSheet As String : 絞り込み対象のデータが記載されているシート
'   CondSheet As String : 絞り込み条件が記載されているシート
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function NarrowDownByFields(ByVal TargetSheet As String, _
                                   ByVal CondSheet As String) As Boolean
  Const strField As String = "Linked Issues"
  Dim vSearchStr As Variant
  Dim i As Long


  Debug.Print "(NarrowDownByFields):TargetSheet = " & TargetSheet & _
              " CondSheet = " & CondSheet

  ' 絞り込み検索対象パラメータ(フィールド名/検索文字列)を取得
  If (GetSearchSetting(CondSheet, strField) = False) Then
    NarrowDownByFields = False
    Exit Function
  End If

  ' 検索文字列が指定されていない場合、処理せずに正常終了する
  If (gSettingFID.SearchStr = "") Then
    Debug.Print "(NarrowDownByFields): Skip processing."
    NarrowDownByFields = True
    Exit Function
  End If

  ' 検索文字列を解析
  vSearchStr = Split(gSettingFID.SearchStr, vbLf)

  If (RunNarrowDown(TargetSheet, vSearchStr) = False) Then
    Debug.Print "(NarrowDownByFields): RunNarrowDown Failed."
    NarrowDownByFields = False
    Exit Function
  End If

  NarrowDownByFields = True

End Function

' 絞り込み検索対象パラメータを取得するFunction
'
' [引数]
'   SheetName As String : 対象シート名
'   FieldName As String : 対象フィールド(見出し)名
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗)
Public Function GetSearchSetting(ByVal SheetName As String, _
                                 ByVal FieldName As String) As Boolean
  Dim wsRtn As Worksheet
  Dim FoundCell As Range
  Dim rowNum As Long
  Dim colNum As Long
  Dim rowTarget As Long


  Debug.Print "(GetSearchSetting):SheetName = " & SheetName & _
              " FieldName = " & FieldName

  Set wsRtn = ActiveSheet

  ' 対象シートが存在しない場合、エラー終了。
  If (isSheetExist(SheetName) = False) Then
    GetSearchSetting = False
    Debug.Print "(GetSearchSetting):[" & SheetName & "] sheet is not exist."
    Exit Function
  End If

  ' 対象シートに指定フィールド(見出し)が無い場合、エラー終了。
  If (GetHeadPos(SheetName, FieldName, _
                 rowNum, colNum) = False) Then
    GetSearchSetting = False
    Debug.Print "(GetSearchSetting):[" & FieldName & "] field is not found."
    Exit Function
  End If

  Worksheets(SheetName).Activate
  ' 選択されているFIDで検索
  Set FoundCell = Cells.Find(what:=gSettingFID.Id, LookAt:=xlWhole)
  If (FoundCell Is Nothing) Then
    GetSearchSetting = False
    Debug.Print "(GetSearchSetting):[" & gSettingFID.Id & "] FID is not found."
    Exit Function
  End If

  rowTarget = FoundCell.Row
  Debug.Print "(GetSearchSetting):rowTarget = " & rowTarget

  gSettingFID.SearchField = FieldName
  gSettingFID.SearchStr = Cells(rowTarget, colNum).Value

  Debug.Print "(GetSearchSetting):gSettingFID.SearchField = " & _
              gSettingFID.SearchField & vbCrLf & _
              "                   gSettingFID.SearchStr = " & gSettingFID.SearchStr

  ' 元のシートに切り替え
  wsRtn.Activate

  GetSearchSetting = True

End Function

' 指定条件でチケットを絞り込むFunction
'
' [引数]
'   TargetSheet As String : 絞り込み対象のデータが記載されているシート
'   Cond As String : 絞り込み条件文字列
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function RunNarrowDown(ByVal TargetSheet As String, _
                              ByVal vCond As Variant) As Boolean
  Const START_ROW As Long = 2
  Dim wsTarget As Worksheet
  Dim rowMax As Long
  Dim rowNum As Long
  Dim colNum As Long
  Dim i As Long
  Dim strPattern(1) As String


  Debug.Print "(RunNarrowDown):TargetSheet = " & TargetSheet
  For i = 0 To UBound(vCond)
    Debug.Print "  vCond(" & i & ") = " & vCond(i)
  Next i

  ' ３個以上の条件指定の場合、エラーを返す。
  If (UBound(vCond) > 1) Then
    Debug.Print "(RunNarrowDown):Error! UBound(vCond) = " & UBound(vCond)
    RunNarrowDown = False
    Exit Function
  End If

  Set wsTarget = Worksheets(TargetSheet)
  ' 1列目の最終行を取得
  rowMax = wsTarget.Cells(Rows.Count, 1).End(xlUp).Row

  ' 対象シートに指定フィールド(見出し)が無い場合、エラー終了
  If (GetHeadPos(TargetSheet, gSettingFID.SearchField, _
                 rowNum, colNum) = False) Then
    Debug.Print "(RunNarrowDown):[" & gSettingFID.SearchField & "] field is not found."
    RunNarrowDown = False
    Exit Function
  End If

  Debug.Print "(RunNarrowDown):rowMax = " & rowMax & _
              " gSettingFID.SearchField = " & gSettingFID.SearchField & _
              " rowNum = " & rowNum & " colNum = " & colNum

  With wsTarget
    ' 検索条件が１個の場合
    If (LBound(vCond) = UBound(vCond)) Then
      ' 先頭に"not"が見つかった場合(Exclude)
      If (InStr(1, vCond(0), "not ", vbTextCompare) = 1) Then
        strPattern(0) = "<>" & Mid(vCond(0), Len("not ") + 1)
      ' 先頭に"not"が見つからなかった場合(Pickup)
      Else
        strPattern(0) = vCond(0)
      End If
      ' Empty -> ""に置換
      strPattern(0) = Replace(strPattern(0), "Empty", "", 1, 1, vbTextCompare)
      
      Debug.Print "(RunNarrowDown):strPattern(0) = " & strPattern(0)

      .Range("A1").AutoFilter colNum, strPattern(0)

    ' 検索条件が２個の場合
    ElseIf (UBound(vCond) = 1) Then
      For i = 0 To UBound(vCond)
        ' 先頭に"not"が見つかった場合(Exclude)
        If (InStr(1, vCond(i), "not ", vbTextCompare) = 1) Then
          strPattern(i) = "<>" & Mid(vCond(i), Len("not ") + 1)
        ' 先頭に"not"が見つからなかった場合(Pickup)
        Else
          strPattern(i) = vCond(i)
        End If
        ' Empty -> ""に置換
        strPattern(i) = Replace(strPattern(i), "Empty", "", 1, 1, vbTextCompare)
      Next i

      Debug.Print "(RunNarrowDown):strPattern(0) = " & strPattern(0) & _
                  " strPattern(1) = " & strPattern(1)

      .Range("A1").AutoFilter colNum, strPattern(0), xlAnd, strPattern(1)

    End If
  End With

  RunNarrowDown = True

End Function

' FID List Tableを取得するFunction
'
' [引数]
'   なし
' [戻り値]
'   ListObject : FID List Table
'
' [処理概要]
'   FID List Tableを取得する。
Public Function GetFIDListTable() As ListObject
  Dim ws  As Worksheet

  Set GetFIDListTable = Nothing
  Set ws = GetWorkSheetByName(gFIDListSheetName, ThisWorkbook)

  If (ws Is Nothing) Then
    Exit Function
  End If

  Set GetFIDListTable = GetTableByName(gFIDListTable, ws)
  Debug.Print "(GetFIDListTable):GetFIDListTable.Name = " & GetFIDListTable.Name
End Function

' 選択されたFIDに対応するFilter Nameを取得するFunction
'
' [引数]
'   Target As Range : FIDが記載されている指定range
' [戻り値]
'   String : Filter Name
'
' [処理概要]
'   指定Rangeの値(FID)に対応するFilter Nameを取得して返す。
Public Function PickUpFilterName(Target As Range) As String
  Dim strFID As String
  Dim strFilterName As String

  PickUpFilterName = ""

  ' FID取得
  strFID = Target.Value
  Debug.Print "(PickUpFilterName):strFID = " & strFID

  strFilterName = GetFilterNameOfFID(strFID)
  If (strFilterName = "") Then
    Exit Function
  End If

  PickUpFilterName = strFilterName
End Function

' 指定FIDに対応するFilter Nameを取得するFunction
'
' [引数]
'   strFID As String : 指定FID文字列
' [戻り値]
'   String : Filter Name
'
' [処理概要]
'   指定FIDに対応するFilter Nameをリストから取得する。
Public Function GetFilterNameOfFID(ByVal strFID As String) As String
  Dim objList As ListObject
  Dim objListRow As ListRow
  Dim objListCol As ListColumn
  Dim rowFound As Long
  Dim c1 As Range
  Dim strFilterName As String
  Dim i As Long

  GetFilterNameOfFID = ""

  Set objList = GetFIDListTable()

  rowFound = 0
  Set objListCol = objList.ListColumns("FID")

  For Each c1 In objListCol.DataBodyRange.Cells
    If (Trim(c1.Value) = strFID) Then
      rowFound = c1.Row - objListCol.DataBodyRange.Row + 1
      Exit For
    End If
  Next c1

  Debug.Print "(GetFilterNameOfFID):rowFound = " & rowFound

  If (rowFound = 0) Then
    Set c1 = objListCol.Range.Cells(1)
    MsgBox "設定エラー at " & c1.Address & vbCrLf & _
           "'" & strFID & "' が未記載です。", vbCritical
    GoTo Exit_Proc
  End If

  Set objListRow = objList.ListRows(rowFound)
  Set objListCol = objList.ListColumns("Filter Name")
  Set c1 = objListRow.Range.Columns(objListCol.Index)

  strFilterName = MySpaceCharsReplace(c1.Value, "")
  Debug.Print "(GetFilterNameOfFID):strFilterName = " & strFilterName

  If (strFilterName = "") Then
    MsgBox "設定エラー at " & c1.Address & vbCrLf & _
           "'" & strFID & "' の" & objListCol.Name & "が未記載です。", vbCritical
    GoTo Exit_Proc
  End If

Normal_End:
  GetFilterNameOfFID = strFilterName

Exit_Proc:
  Set objListRow = Nothing
  Set objListCol = Nothing
  Set objList = Nothing
End Function

'--------------------------------------------------------------
' DLシート変更イベントの処理
'--------------------------------------------------------------
Public Sub control_ws_change(Target As Range)
  Dim r1 As Range
  Dim varSave As Variant: varSave = Application.EnableEvents
  Dim strFilterName As String

  Set r1 = GetRefNameRange(Worksheets(gConfigSheetName), gFIDRangeDef)

  If r1 Is Nothing Then
    Exit Sub
  End If

  If Target.Address(External:=True) = r1.Address(External:=True) Then
    strFilterName = PickUpFilterName(r1)

    Application.EnableEvents = False

    With Worksheets(gConfigSheetName)
      If (strFilterName = "") Then
        .Range(gFilterNameRangeDef).ClearContents
      Else
        .Range(gFilterNameRangeDef).Value = strFilterName
      End If
    End With

    Application.EnableEvents = varSave
  End If

  Set r1 = Nothing
End Sub

