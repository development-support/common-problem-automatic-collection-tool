Attribute VB_Name = "IntegrateData"
Option Explicit

Public Const gTargetDateRangeDef As String = "TargetDate"
Public Const gStartDateRangeDef As String = "StartDate"
Public Const gEndDateRangeDef As String = "EndDate"
Public Const gTempSheetName As String = "TempData"
Public Const gIntegSheetName As String = "All_"

Type tIntegSet
  TotalFid As Long           ' 「FID_List」に記載の全フィルターID数
  Fid() As Variant           ' 全フィルターID値
  Targetdate As String       ' 統合対象データの日付
  StartDate As String        ' 絞り込み開始日付(for Resolved field)
  EndDate As String          ' 絞り込み終了日付(for Resolved field)
  TotalSheet As Long         ' Targetdateを含むシート名の総数
  TargetSheet() As Worksheet ' Targetdateを含むシートオブジェクト
End Type

Public gIntegSet As tIntegSet


'*******************************************************************************
' JIRAデータ統合処理マクロ
'
' [引数]
'   なし
'
' [戻り値]
'   なし
'
' [処理概要]
'   (Step 1)「FID_List」シートにある全FID値を取得する。
'           ※取得不可の場合、強制終了
'   (Step 2)Target date, Start/End Resolved Dateの値を取得する。
'           ※取得不可の場合、強制終了
'   (Step 3)Target dateに対応するシートが存在するかチェックする。
'           ※対応するシートが１つも無い場合、強制終了
'   (Step 4)ユーザーフォームに、統合対象の存在する全シート名を列挙し、統合処理を
'           行うかを問う(Yes or No)。※Noの場合、この時点で処理終了。
'   (Step 5)存在する全シートを統合し、一時出力用シートに出力する。
'   (Step 6)一時出力用シート上で、Resolvedフィールドに対して、指定されたStart/End
'           日付の条件でデータを絞り込む。
'   (Step 7)絞り込みんだデータを出力用シートにコピーする。
'           ※一時出力用シートは削除する。
'   (Step 8)出力用シートに、見出し「問題内容 / 顧客影響」(D列)、「重要度」(E列)
'           を追加する。
'*******************************************************************************
Public Sub IntegrateJiraData(Optional ByVal bMsg As Boolean = True)
  Dim wsRtn As Worksheet
  Dim rc As VbMsgBoxResult
  Dim strMsg As String
  Dim i As Long
  Dim IntegSheet As String


  Application.ScreenUpdating = False
  Application.Calculation = xlCalculationManual

  Set wsRtn = ActiveSheet

  ' (Step 1)「FID_List」シートにある全FID値を取得する。
  If (GetAllFid(gFIDListSheetName) = False) Then
    ' ※取得不可の場合、強制終了
    MsgBox "[" & gFIDListSheetName & "]シートが存在しないか、" & vbCrLf & _
           "シート上にFIDの記載がありません。", vbCritical
    Exit Sub
  End If

  ' (Step 2)Target date, Start/End Resolved Dateの値を取得する。
  If (GetIntegrateInfo() = False) Then
    ' ※取得不可の場合、強制終了
    MsgBox "Target Dateに日付を指定して下さい。", vbCritical
    Exit Sub
  End If

  ' (Step 3)Target dateに対応するシートが存在するかチェックする。
  If (CheckTargetSheets(gIntegSet.Targetdate) = False) Then
    ' ※対応するシートが１つも無い場合、強制終了
    MsgBox "Target Date [" & gIntegSet.Targetdate & _
           "] に対応するシートが１つもありません。" & vbCrLf & _
           "Target Dateを見直すか、対応するデータを取得して下さい。", vbCritical
    Exit Sub
  End If

  ' (Step 4)ユーザーフォームに、統合対象の存在する全シート名を列挙し、統合処理を
  '         行うかを問う(Yes or No)。※Noの場合、この時点で処理終了。
  For i = 1 To gIntegSet.TotalSheet
    If (i = 1) Then
      strMsg = gIntegSet.TargetSheet(i).Name
    Else
      strMsg = strMsg & vbCrLf & gIntegSet.TargetSheet(i).Name
    End If
  Next i

  strMsg = strMsg & vbCrLf & "上記のシートを１シートに統合します。よろしいですか？"

  If (bMsg = True) Then
    rc = MsgBox(strMsg, vbYesNo + vbQuestion)
  Else
    rc = vbYes
  End If

  If (rc = vbYes) Then
    ' (Step 5)存在する全シートを統合し、一時出力用シートに出力する。
    Call DoIntegrateSheets(gTempSheetName)
    Call SetWsParam(Worksheets(gTempSheetName))
  Else
    Exit Sub
  End If

  ' (Step 6)一時出力用シート上で、Resolvedフィールドに対して、指定されたStart/End
  '         日付の条件でデータを絞り込む。
  ' 開始/終了日付共に指定が無い場合
  If (gIntegSet.StartDate = "") And (gIntegSet.EndDate = "") Then
    ' NOP
  ' 開始日が日付変換不可な文字列の場合
  ElseIf (gIntegSet.StartDate <> "") And (IsDateEx(gIntegSet.StartDate) = False) Then
    MsgBox "Start Resolved Dateの日付指定が不正です。絞り込み処理はSKIPされます。", _
           vbExclamation
  ' 開始日が日付変換不可な文字列の場合
  ElseIf (gIntegSet.EndDate <> "") And (IsDateEx(gIntegSet.EndDate) = False) Then
    MsgBox "End Resolved Dateの日付指定が不正です。絞り込み処理はSKIPされます。", _
           vbExclamation
  Else
    If (NarrowDownByDate(gTempSheetName, _
                         gIntegSet.StartDate, _
                         gIntegSet.EndDate) = False) Then
      MsgBox "一時出力シート [" & gTempSheetName & "] のフォーマットが異常のため終了します。"
      Exit Sub
    End If
  End If

  ' (Step 7)絞り込みんだデータを出力用シートにコピーする。
  IntegSheet = gIntegSheetName & gIntegSet.Targetdate
  Debug.Print "(IntegrateJiraData):IntegSheet = " & IntegSheet

  Call CreateSheet(IntegSheet)
  Call CopySheetRange(gTempSheetName, IntegSheet)
  Call SetWsParam(Worksheets(IntegSheet))
  ' ※一時出力用シートは削除する。
  Application.DisplayAlerts = False
  Worksheets(gTempSheetName).Delete
  Application.DisplayAlerts = True

  ' (Step 8)出力用シートに、見出し「問題内容 / 顧客影響」(D列)、「重要度」(E列)
  '         を追加する。
  Call InsertHeading(IntegSheet)

  Application.Calculation = xlCalculationAutomatic
  Application.ScreenUpdating = True
  ' 元のシートへ戻る
  wsRtn.Activate

  If (bMsg = True) Then
    MsgBox "Processing completed.", vbInformation
  End If

End Sub

' 指定シートから全FIDを取得するFunction
'
' [引数]
'   SheetName As String : 対象シート名
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗)
Public Function GetAllFid(ByVal SheetName As String) As Boolean
  Dim wsRtn As Worksheet
  Dim i As Long


  Debug.Print "(GetAllFid):SheetName = " & SheetName
  Set wsRtn = ActiveSheet

  ' 対象シートが存在しない場合、エラー終了。
  If (isSheetExist(SheetName) = False) Then
    GetAllFid = False
    Debug.Print "(GetAllFid):[" & SheetName & "] sheet is not exist."
    Exit Function
  End If

  Worksheets(SheetName).Activate

  ' FID_Listシートのリストから全FID値を取得。
  If (GetAllValueFromHeading("FID", gIntegSet.Fid()) = False) Then
    GetAllFid = False
    Debug.Print "(GetAllFid):Error in GetAllValueFromHeading."
    Exit Function
  End If
  ' 全FID値の総数を保存
  gIntegSet.TotalFid = UBound(gIntegSet.Fid)

  Debug.Print "(GetAllFid):gIntegSet.TotalFid = " & gIntegSet.TotalFid
  For i = 1 To UBound(gIntegSet.Fid)
    Debug.Print "  gIntegSet.Fid(" & i & ") = " & gIntegSet.Fid(i)
  Next i

  ' 元のシートに切り替え
  wsRtn.Activate

  GetAllFid = True

End Function

' 指定見出しに記載されている全ての値を取得するFunction
'
' [引数]
'   HeadingName As String : 見出し名
'   AllValue() As Variant : 指定した見出し名列に記載されている全ての値
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function GetAllValueFromHeading(ByVal HeadingName As String, _
                                       ByRef vAllValue() As Variant) As Boolean
  Dim objList As ListObject
  Dim objListRow As ListRow
  Dim objListCol As ListColumn
  Dim TotalRow As Long
  Dim c1 As Range
  Dim i As Long


  Debug.Print "(GetAllValueFromHeading):HeadingName = " & HeadingName
  Set objList = GetFIDListTable()
  If (objList Is Nothing) Then
    GetAllValueFromHeading = False
  End If

  Set objListCol = objList.ListColumns("FID")
  ' 全データ行数を取得
  TotalRow = objList.ListRows.Count
  Debug.Print "(GetAllValueFromHeading):TotalRow = " & TotalRow

  ReDim vAllValue(TotalRow)

  i = 1
  For Each c1 In objListCol.DataBodyRange.Cells
    vAllValue(i) = Trim(c1.Value)
    i = i + 1
  Next c1

  Set objListRow = Nothing
  Set objListCol = Nothing
  Set objList = Nothing

  GetAllValueFromHeading = True

End Function

' 設定パラメータ(統合用)を取得するFunction
'
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗)
Public Function GetIntegrateInfo() As Boolean

  With Worksheets(gConfigSheetName)
    gIntegSet.Targetdate = .Range(gTargetDateRangeDef).Value
    gIntegSet.StartDate = .Range(gStartDateRangeDef).Value
    gIntegSet.EndDate = .Range(gEndDateRangeDef).Value
  End With

  Debug.Print "(GetIntegrateInfo):gIntegSet.TargetDate = " & gIntegSet.Targetdate & vbCrLf & _
              "                   gIntegSet.StartDate = " & gIntegSet.StartDate & vbCrLf & _
              "                   gIntegSet.EndDate = " & gIntegSet.EndDate

  ' **** 実行条件チェック ****
  ' Target Dateが指定されていない場合、エラー終了
  If (gIntegSet.Targetdate = "") Then
    GetIntegrateInfo = False
    Exit Function
  End If

  GetIntegrateInfo = True

End Function

' 指定日付がシート名に含まれているシートを取得するFunction
'
' [引数]
'   Targetdate As String : 統合対象のシート名に含まれている日付
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗)
Public Function CheckTargetSheets(ByVal Targetdate As String) As Boolean
  Dim ws As Worksheet
  Dim strTarget As String
  Dim i As Long


  Debug.Print "(CheckTargetSheets):Targetdate = " & Targetdate
  strTarget = "_" & Targetdate
  i = 0

  For Each ws In Worksheets
    If (InStr(ws.Name, strTarget) > Len(gIntegSheetName)) Then
      i = i + 1
      ReDim Preserve gIntegSet.TargetSheet(i)
      Set gIntegSet.TargetSheet(i) = ws
    End If
  Next ws

  gIntegSet.TotalSheet = i

  ' 対応するシートが１つも見つからなかった場合、エラー
  If (i = 0) Then
    CheckTargetSheets = False
    Exit Function
  End If

  Debug.Print "(CheckTargetSheets):gIntegSet.TotalSheet = " & gIntegSet.TotalSheet
  For i = 1 To gIntegSet.TotalSheet
    Debug.Print "  gIntegSet.TargetSheet(" & i & ").Name = " & _
                   gIntegSet.TargetSheet(i).Name
  Next i

  CheckTargetSheets = True

End Function

' 統合対象の全シートを指定シートに出力するFunction
'
' [引数]
'   OutputSheet As String : 統合データを出力するシート名
'
' [戻り値]
'   なし
Public Function DoIntegrateSheets(ByVal OutputSheet As String)
  Dim wsOut As Worksheet
  Dim i As Long


  Debug.Print "(DoIntegrateSheets):OutputSheet = " & OutputSheet
  ' 指定シートを作成
  Call CreateSheet(OutputSheet)

  Set wsOut = Worksheets(OutputSheet)

  For i = 1 To gIntegSet.TotalSheet
    If (i = 1) Then
      Call CopySheetRange(gIntegSet.TargetSheet(i).Name, OutputSheet)
    Else
      Call CopyWs2Ws(gIntegSet.TargetSheet(i), wsOut)
    End If
  Next i

End Function

' Resolvedフィールドに対し、指定日付の範囲でチケットを絞り込むFunction
'
' [引数]
'   SheetName As String : 絞り込み対象シート名
'   StartDate As String : 絞り込み開始日付
'   EndDate As String : 絞り込み終了日付
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function NarrowDownByDate(ByVal SheetName As String, _
                                 ByVal StartDate As String, _
                                 ByVal EndDate As String) As Boolean
  Dim wsND As Worksheet
  Dim rowNum As Long
  Dim colNum As Long


  Debug.Print "(NarrowDownByDate):SheetName = " & SheetName & _
              " StartDate = " & StartDate & " EndDate = " & EndDate

  Set wsND = Worksheets(SheetName)

  ' 絞り込み対象フィールドが無い場合、エラー終了
  If (GetHeadPos(SheetName, "Resolved", _
                 rowNum, colNum) = False) Then
    NarrowDownByDate = False
    Exit Function
  End If

  Debug.Print "(NarrowDownByDate):rowNum = " & rowNum & _
              " colNum = " & colNum

  ' 開始日付のみ指定
  If (StartDate <> "") And (EndDate = "") Then
    wsND.Range("A1").AutoFilter colNum, ">=" & StartDate
  ' 終了日付のみ指定
  ElseIf (StartDate = "") And (EndDate <> "") Then
    wsND.Range("A1").AutoFilter colNum, "<=" & EndDate
  ' 開始/終了日付を指定
  Else
    wsND.Range("A1").AutoFilter colNum, ">=" & StartDate, xlAnd, "<=" & EndDate
  End If

  NarrowDownByDate = True

End Function

' 指定シートにカスタム見出しを追加するFunction
'
' [引数]
'   SheetName As String : カスタム見出しを追加するシート名
'
' [戻り値]
'   なし
Public Function InsertHeading(ByVal SheetName As String)
  Const strColD As String = "問題内容 / 顧客影響"
  Const strColE As String = "重要度"
  Const strValidateList As String = "高,中,低,対象外"
  Dim ws As Worksheet
  Dim rowMax As Long
  Dim strRange As String


  Debug.Print "(InsertHeading):SheetName = " & SheetName
  Set ws = Worksheets(SheetName)

  With ws
    ' 「問題内容 / 顧客影響」(D列)、「重要度」(E列)を挿入
    .Columns("D:E").Insert
    .Range("D1").Value = strColD
    .Range("E1").Value = strColE

    rowMax = .UsedRange.Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    Debug.Print "(InsertHeading):rowMax = " & rowMax
    
    strRange = "E2:E" & rowMax
    With .Range(strRange).Validation
      .Delete
      .Add Type:=xlValidateList, AlertStyle:=xlValidAlertWarning, _
           Operator:=xlEqual, Formula1:=strValidateList
    End With
  End With

End Function
